/*Known issues: Will not work properly with jQuery versions (below 1.8) that don'thandle box-sizing correctly*/
(function($){
	
	var settings = {};
		
		_sampGallery = function(_elem){

			var _scrSizes = {'xs':480, 'sm':767, 'md': 768},  _curItemOffset = {top:-1, left:-1}, _curThbDims = {'w':0,'h':0}, _curImgDims = {'w':0,'h':0}, _curPreviewDims = {'w':0,'h':0}, _curElemDims = {'w':_elem.outerWidth(),'h':_elem.outerHeight()};//store current thumb offset

            function realImgDimension(img) {
                var i = new Image();
                i.src = img.src;

                return {
                    w: parseInt(i.width),
                    h: parseInt(i.height)
                };
            }

            /*function doScroll(item){
                var itemOffset = jQuery(item).offset();
                    if(settings.scrolltoitem ){
                        var scrlTo = itemOffset.top-settings.scrolloffset.top;

                            if(_curElemDims.w < _scrSizes.sm){
                                scrlTo = itemOffset.top+((jQuery(item).height()/4)*3);//leave 1/4 of thumb visible
                            }

                            jQuery('html, body').stop().animate({scrollTop: scrlTo+'px'}, settings.animationspeed);
                        _curItemOffset = itemOffset;
                    }
            }*/


            function buildControls(url){
                    if(_elem.find('.sampgallery-ctrls').length > 0) _elem.find('.sampgallery-ctrls').remove();
                var cHtml = '<div class="sampgallery-ctrls">';
                cHtml += '<button class="sampgallery-ctrl sampgallery-ctrl-close" role="button"></button>';
                cHtml += '<button class="sampgallery-ctrl sampgallery-ctrl-zoomin" role="button"></button>';
                cHtml += '<button class="sampgallery-ctrl sampgallery-ctrl-zoomout" role="button"></button>';
                cHtml += '<a class="sampgallery-ctrl sampgallery-ctrl-openimage" href="'+url+'" target="_blank"></a>';
                cHtml += '</div>';
                return cHtml;
            }


            function openPreview(_this){

                _elem = jQuery(_this.parentElement);
                //var cTotal = _elem.find('.sampgallery-thumb').length;//var _thisIndex = jQuery(_this).index();
                _curThbDims.w = jQuery(_this).outerWidth();
                _curThbDims.h = jQuery(_this).outerHeight();
                jQuery(_this).addClass('sampgallery-loading sampgallery-selected');
                var _thisOffset = jQuery(_this).offset();
                var prevItem = _elem.children().first();
                var cLast = _elem.find('.sampgallery-thumb').last();
                var lastOffset = cLast.offset();
                var curPreview = _elem.find('.sampgallery-preview-block');
                var _thisImg = new Image();
                _thisImg.src = jQuery(_this).find(' > figure img').attr('data-full-url');

                    _thisImg.addEventListener('load', function() {

                        _curImgDims = realImgDimension(this);
                        var fullImgPath = this.src;
                        var caption = '';

                            if(jQuery(_this).find(' > figure > figcaption').text().length > 0) caption = '<p class="sampgallery-preview-caption">'+jQuery(_this).find(' > figure > figcaption').html()+'</p>';

                            //if(typeof jQuery(_this).attr('data-fullsize') !== 'undefined') fullImgPath = jQuery(_this).attr('data-fullsize');
                        var oHtml = '<div class="sampgallery-preview-block"><div class="sampgallery-preview">' + buildControls(fullImgPath) + '<a href="' + fullImgPath + '" target="_blank"><img src="' + fullImgPath + '"></a></div>' + caption + '</div>';

                        _elem.find('.sampgallery-thumb').each(function(i, item){

                            var itemOffset = jQuery(item).offset();

                                if(itemOffset.top > _thisOffset.top || _thisOffset.top == lastOffset.top){

                                    var lastItem = prevItem;

                                        if( _thisOffset.top == lastOffset.top){
                                            lastItem = cLast;
                                        }

                                        if((_curItemOffset.top === _thisOffset.top) && typeof curPreview !== 'undefined' && curPreview.length > 0){
                                            //console.log('HERE!', oHtml);
                                            //if(typeof curPreview !== 'undefined' && curPreview.length > 0){
                                            //curPreview.prepend(buildControls(fullImgPath)).find('a').attr('href', fullImgPath).find('img').attr('src', fullImgPath);
                                            //curPreview.find(' + .sampgallery-preview-caption').replaceWith(caption);
                                            curPreview.replaceWith(oHtml);

                                            previewFitWindow(function(){
                                                    jQuery(_this).addClass('sampgallery-selected');
                                                //doScroll(_this);
                                            }, null);

                                        }else{

                                                if(typeof curPreview != 'undefined' && curPreview.length > 0){

                                                    closePreview(function(){

                                                            jQuery(lastItem).after(oHtml);

                                                        previewFitWindow(function(){
                                                                _elem.find('.sampgallery-thumb').removeClass('sampgallery-loading');
                                                                jQuery(_this).addClass('sampgallery-selected');
                                                            //doScroll(_this);
                                                        }, null);
                                                    }, null);

                                                }else{

                                                        jQuery(lastItem).after(oHtml);

                                                    previewFitWindow(function(){
                                                        _elem.find('.sampgallery-thumb').removeClass('sampgallery-loading');
                                                        jQuery(_this).addClass('sampgallery-selected');
                                                        //doScroll(_this);
                                                    }, null);
                                                }
                                        }

                                    return false;
                                }
                            prevItem = item;
                        });
                        _curItemOffset = _thisOffset;
                    });

                //_thisOffset = prevItem = cLast = lastOffset = curPreview =
                _thisImg = null;
            }


            function closePreview(callback, args){

                var thisPreview = _elem.find('.sampgallery-preview-block');
                var thisItem = _elem.find('.sampgallery-thumb.sampgallery-selected');

                    if(typeof thisItem.offset() != 'undefined') _curItemOffset = thisItem.offset().top-thisPreview.offset().top;

                    if(thisPreview.length > 0){

                        thisPreview.stop().animate({'height':'0px'}, settings.animationspeed, function(){
                                thisPreview.remove();
                                if(typeof callback === 'function') callback(args);
                        });

                        _elem.find('.sampgallery-thumb').removeClass('sampgallery-selected');
                    }
            }


            function  ctrlHandler(type){

                var tC = _elem.find('.sampgallery-preview').find('.sampgallery-ctrls');

                    tC.find('.sampgallery-ctrl-zoomin, .sampgallery-ctrl-zoomout').css('display','none');

                    /*if(_curPreviewDims.w > _scrSizes.sm){
                        if(_curImgDims.h > _curPreviewDims.h){
                                tC.find('.sampgallery-ctrl-zoomin').css('display','block');
                        }else if(_curImgDims.h < _curPreviewDims.h){
                                //tC.find('.sampgallery-ctrl-zoomout').css('display','block');
                        }
                    }*/
            }


            function previewFitWindow(cb,args){

                var toHeight = calcFitWindow()['h'];

                    /*if(_curElemDims.w < _scrSizes.sm){
                        toHeight = calcFitPreviewer()['h'];
                    }*/

                    _elem.find('.sampgallery-preview').animate({'height': toHeight}, settings.animationspeed, function(){
                        _elem.find('.sampgallery-thumb').removeClass('sampgallery-loading');
                        ctrlHandler('zoomin');
                            if(typeof cb === 'function') cb(args);
                    });
            }


            function previewZoomin(cb,args){

                    _elem.find('.sampgallery-preview').animate({'height': calcFitPreviewer()['h']}, settings.animationspeed, function(){
                        ctrlHandler('zoomout');
                            if(typeof cb === 'function') cb(args);
                    });
            }


            function calcFitWindow(){
                //var previewDims = {'w':_elem.outerWidth(),'h':jQuery(window).height()-_curThbDims.h-settings.scrolloffset.top-settings.scrolloffset.bottom};
                var previewDims = {w:_elem.outerWidth(), h: Math.round(_curImgDims.h*(_elem.width()/_curImgDims.w))};

                //if portrait
                    if(_curImgDims.h > _curImgDims.w){

                        previewDims.h = Math.round(_curImgDims.w*(_elem.height()/_curImgDims.h));

                    }

                    if(previewDims.h > _curImgDims.h) previewDims.h =  _curImgDims.h;

                var ch = _elem.find('.sampgallery-preview > .sampgallery-ctrls').outerHeight();

                    if(previewDims.h < ch) {
                        previewDims.h = ch;
                    }

                _curPreviewDims = previewDims;
                ch = null;
                return previewDims;
            }


            function calcFitPreviewer(){
                var prv = _elem.find('.sampgallery-preview');
                var pad = parseInt(prv.outerHeight()-prv.height());
                var rat = _curImgDims.w/(_curElemDims.w-pad);
                var zD = {'w': prv.outerWidth()};

                    if(_curImgDims.w > _curElemDims.w-pad) zD.w = _curElemDims.w-pad;

                    if(zD.w > _curImgDims.w) zD.w = _curImgDims.w;

                    if(_curImgDims.w === _curImgDims.h) rat = 1;//Not a mathematician so dunno why square images go weird

                zD.h = (_curImgDims.h/rat)+pad;
                return zD;
            }


            var resizeTimer;

            jQuery(window).on('resize', function(){

                    if(resizeTimer)	clearTimeout(resizeTimer);
                    //if element changes more than half thumb width remove preview and reset thumbs
                    if(Math.abs(_curElemDims.w-_elem.outerWidth()) > _elem.find('.sampgallery-thumb').outerWidth()/2){

                        resizeTimer = setTimeout(function() {
                            _curElemDims = {'w':_elem.outerWidth(),'h':_elem.outerHeight()};
                                _elem.find('.sampgallery-preview-block').remove();
                                _elem.find('.sampgallery-thumb').removeClass('sampgallery-loading sampgallery-selected');
                        }, 250);
                    }
            });


            _elem.on('click', '.sampgallery-thumb', function(evt){

                evt.preventDefault();
                _elem = jQuery(this.parentElement);

                    if(jQuery(this).hasClass('sampgallery-selected')){
                        closePreview(null, null);
                        return false;
                    }else{
                        _elem.find('.sampgallery-thumb').removeClass('sampgallery-selected');
                        openPreview(this);
                    }
            });


            _elem.on('click', '.sampgallery-ctrl-close', function(evt){
                evt.preventDefault();
                _elem = jQuery(this.parentElement.parentElement.parentElement.parentElement);
                closePreview(null, null);
            });


            _elem.on('click', '.sampgallery-ctrl-zoomin', function(evt){
                evt.preventDefault();
                _elem = jQuery(this.parentElement.parentElement.parentElement.parentElement);
                var zi = jQuery(this);
                previewZoomin(function(){
                        if(_curPreviewDims.w > _scrSizes.sm){
                                _elem.find('.sampgallery-ctrls .sampgallery-ctrl').css('display','block');
                            zi.hide();
                        }
                }, null);
            });


            _elem.on('click', '.sampgallery-ctrl-zoomout', function(evt){
                evt.preventDefault();
                _elem = jQuery(this.parentElement.parentElement.parentElement.parentElement);
                var zo = jQuery(this);
                previewFitWindow(function(){
                        if(_curPreviewDims.w > _scrSizes.sm){
                                _elem.find('.sampgallery-ctrls .sampgallery-ctrl').css('display','block');
                            zo.hide();
                        }
                }, 'zoomout');
            });


			    if(typeof settings.afterinit === 'function') settings.afterinit();

		}
        
        
		function _setupSampGallery(_elem){

            _elem.find('ul').addClass('sampgallery').find(' > li').each(function(i, item){

                    if(settings.showintro){
                            jQuery(item).addClass('sampgallery-thumb sampgallery-thumb-intro').delay(i*250).animate({'opacity':1}, settings.animationspeed, function(){
                                     jQuery(item).removeClass('sampgallery-thumb-intro');
                            });
                    }else{
                            jQuery(item).addClass('sampgallery-thumb');
                    }
            });

            _elem[0].classList.add('sampgallery-ready');
		}


		jQuery.fn.sampGallery = function(options){			
			settings = jQuery.extend({
				//scrolltoitem: true,//Scroll page to preview
				//scrolloffset: {top:0, bottom:0},//if needed to add offset, for example for fixed header and footer. Works only when screen is larger  than 767 pixels
				animationspeed: 300,
				//thumbscaled: true,//use thumbs as background-images
                //keepzoom: true,//keep zoom setting, not yet implemented
                showintro: false,//Boolean, animate photos in, if this is used the element has to have class="sampgallery" added in markup
				afterinit: function(){
					//console.log('Broom.');//function to do after init
				}
			}, options);
				

            this.each(function() {
                //Do something to each element here.
                //console.log(this);
                    if(!this.classList.contains('sampgallery-exclude')) _setupSampGallery(jQuery(this));
            });


			return _sampGallery(this);
		};		
		/*jQuery.fn.sampGallery.defaults = {}; default options end */
})($);